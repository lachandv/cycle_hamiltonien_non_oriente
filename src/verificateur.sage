# -*-coding:Latin-1 -*

## \file verificateur.py
## \package verificateur
## \author Valentin Lachand
# Verificateur d'un problème hamiltonien non-oriente.
#
# Permet de vérifier si un graphe non orienté est hamiltonien.
from sage import graphs
from sys  import argv
Verbose = 1

##
# Avancement du programme :
#


##
# Cette fonction permet de vérifier que la graphe entré en paramètre
# est Hamiltonien grace au certificat donné.
#
# \param g le graphe dont on veut vérifier qu'il est Hamiltonien
# \param ca le certificat pour tester
# \return true si le certificat fonctionne, false sinon
def verificateur(g, ca, mode):

    l = list()

    if (len(ca) != g.num_verts()+1):

        if (mode == Verbose):
            print("Le certificat est trop petit, vérification échouée \n")
        return False

    elif (ca[0] != ca[g.num_verts()]):
        if (mode == Verbose):
            print("Le certificat ne forme pas un cycle, vérification échouée \n")
        return False

    else:
        for i in range(g.num_verts()+1):
            if ca[i] in l:
                if (mode == Verbose):
                    print("Le sommet " + str(ca[i]) + "apparait deux fois dans le chemin, vérification échouée \n")
                return False
            elif i>0 and ca[i] in g[ca[i-1]]: 
                l.append(ca[i])
            elif i> 0 :
                if (mode == Verbose):
                    print("Le certificat est inexact \n")
                return False

    if (mode == Verbose):
        print("Le graphe est Hamiltonien \n")
    return True

def _decode_list(data):
    rv = []
    for item in data:
        if isinstance(item, unicode):
            item = item.encode('utf-8')
        elif isinstance(item, list):
            item = _decode_list(item)
        elif isinstance(item, dict):
            item = _decode_dict(item)
        rv.append(item)
    return rv

def _decode_dict(data):
    rv = {}
    for key, value in data.iteritems():
        if isinstance(key, unicode):
            key = key.encode('utf-8')
        if isinstance(value, unicode):
            value = value.encode('utf-8')
        elif isinstance(value, list):
            value = _decode_list(value)
        elif isinstance(value, dict):
            value = _decode_dict(value)
        rv[key] = value
    return rv
#
if __name__ == '__main__':
    cpt = 1
    mode = 0
    fin = False
    graphe = False
    while cpt<len(argv) and not fin:
        if argv[cpt] == '-h' or argv[cpt] == '--help':
            print("Help for solver.py :")
            print("usage : sage verificateur.py [options] arg") 
            print("Options and arguments :")  
            print("-v --verbose : verbose (trace import statements)")
            print("-h --help    : print this help message and exit")
            print("arg          : a file which represent a simple graph")
            fin = True
        if argv[cpt] == '-v' or argv[cpt] == '--verbose':
            print("Mode verbose :")
            mode = Verbose
            cpt = cpt + 1
        elif graphe == False:
	    import json
            src = open(argv[cpt]).read()
            data = json.loads(src, object_hook=_decode_dict)
            g = Graph()
            g.add_vertices(data['vertices'])
            g.add_edges(data['edges'])
	    graphe = True
	    cpt = cpt + 1
	else :
            src2 = open(argv[cpt]).read()
	    data2 = json.loads(src2, object_hook=_decode_dict)
            ca = data2['ca']
            l = (verificateur(g,ca, mode))
	    print l
	    if l == True :
	       sys.exit("0")
	    else :
	       sys.exit("1")
            fin = True
