#!/usr/bin/env python -W ignore::DeprecationWarning
# -*-coding:Latin-1 -*
import warnings
from sage.all_cmdline import *

with warnings.catch_warnings():
    warnings.filterwarnings("ignore",category=DeprecationWarning)
## \file solver.py
## \package solver
## \author Valentin Lachand
# Solver d'un problème hamiltonien non-oriente.
#
# Permet d'indiquer si un graphe non orienté est hamiltonien.
from sage.all import Graph
from sys  import argv
import verificateur

Verbose = 1
Certificat = 2

##
# Avancement du programme :
#
# Fait et testé :
# Ore
# Dirac
# Posa
# Général



##
# Cette fonction simple regarde s'il existe des sommets "seuls"
#
# \param g le graphe simple dont on cherche un cycle Hamiltonien.
# \return true si le graphe est Hamiltonien, false sinon.
def simple(g, mode) :
    cpt = 0;
    retour = True
    if mode == Verbose :
        print("")
        print("Utilisation d'une fonction simple :")
        print("")

    while retour == True and cpt < len(g):
        if len(g[g.vertices()[cpt]]) == 0:
            if mode == Verbose :
                print("L'arrete n° " + str(cpt) + " est seule : Sortie")
            retour = False
        else :
            if mode == Verbose :
                print("L'arrete n° " + str(cpt) + " possède " + str(len(g[g.vertices()[cpt]])) + " voisins")
            cpt = cpt + 1

    return retour
    

##
# Cette fonction utilise le théorème de Ore pour savoir
# si un graphe simple est Hamiltonien : il l'est si soit
# n son nombre de sommets > 3, la somme des chaque paires
# de sommets non adjacent est au moins n.
#
# \param g le graphe simple dont on cherche un cycle Hamiltonien.
# \return true si le graphe est Hamiltonien, false sinon.
def Ore(g, mode) :

    if mode == Verbose :
        print("")
        print("Utilisation du théorème de Ore :")
        print("")

    n = g.num_verts()
    l = g.degree()
    cpt_sommet = 0
    cpt_sommet_non_adjacent = 0
    retour = True
    
    while retour == True and cpt_sommet < n:
        while retour == True and cpt_sommet_non_adjacent < n:
            if (g.vertices()[cpt_sommet_non_adjacent] in g[g.vertices()[cpt_sommet]]) == False and cpt_sommet_non_adjacent != cpt_sommet:

                if l[cpt_sommet_non_adjacent] + l[cpt_sommet] < n:
                    
                    return False

            cpt_sommet_non_adjacent = cpt_sommet_non_adjacent + 1
        cpt_sommet = cpt_sommet + 1
        cpt_sommet_non_adjacent = 0

    if mode == Verbose and retour == True:
        print("Le graphe est Hamiltonien")
    if mode == Verbose and retour == False:
        print("Le théorème est insuffisant")

    return retour

##
# Cette fonction utilise le théorème de Dirac pour savoir
# si un graphe simple est Hamiltonien : il l'est si soit n son
# nombre de sommets > 3, chaque sommet est au moins de
# degré n/2
#
# Ce théorème est un cas particulier du théorème d'Ore, il
# prend cependant moins de temps lors de la vérification.
#
# \param g le graphe simple dont on cherche a savoir s'il est Hamiltonien.
# \return True si le graphe est Hamiltonien, False sinon.
def Dirac(g, mode) :

    if mode == Verbose :
        print("")
        print("Utilisation du théorème de Dirac :")
        print("")

    n = g.num_verts()
    l = g.degree()
    cpt = 0
    retour = True
    while retour == True and cpt < len(l):
        if float(l[cpt]) < float(n/2.):
            retour = False
            if mode == Verbose :
                print("Sommet n° " + str(cpt) +" degré : " + str(l[cpt]) + " < n/2 : Sortie")
        else :
            if mode == Verbose :
                print("Sommet n° " + str(cpt) +" degré : " + str(l[cpt]))
            cpt = cpt + 1
    
    if mode == Verbose and retour == True:
        print("Le graphe est Hamiltonien")
    if mode == Verbose and retour == False:
        print("Le théorème est insuffisant")

    return retour

##
# Cette fonction utilise le théorème de Posa pour savoir si un
# graphe est Hamiltonien ou non. 
#
# \param g le graphe simple dont on cherche a savoir s'il est Hamiltonien.
# \return True si le graphe est Hamiltonien, False sinon.
def Posa(g, mode):

    if mode == Verbose :
        print("")
        print("Utilisation du théorème de Posa :")
        print("")

    n = g.num_verts()
    liste_degree = g.degree()
    liste_nb_degree = list()
    cpt = 1
    retour = True
    liste_nb_degree.append(0)
    liste_nb_degree.append(liste_degree.count(1))
    for i in range(2,((n-1)/2+1)) :
        liste_nb_degree.append(liste_nb_degree[i-1] + liste_degree.count(i))
    while cpt < (n-1)/2 and retour == True :
        if liste_nb_degree[cpt] <= cpt :
            if mode == Verbose :
                print("Nombre de sommets de degré inférieur ou égal à " + str(cpt) + " : " + str(liste_nb_degree[cpt]) )
            cpt = cpt + 1
        else :
            if mode == Verbose :
                print("Nombre de sommets de degré inférieur ou égal à " + str(cpt) + " : " + str(liste_nb_degree[cpt]) + " c'est superieur à " + str(cpt) + " : Sortie" )
            retour = False

    if retour == True and liste_nb_degree[(n-1)/2] > (n-1)/2 :
        if mode == Verbose :
                print("Nombre de sommets de degré inférieur ou égal à " + str(cpt) + " : " + str(liste_nb_degree[cpt]) + " c'est superieur à " + str(cpt) + " : Sortie" )
        retour = False

    if mode == Verbose and retour == True:
        print("Le graphe est Hamiltonien")
    if mode == Verbose and retour == False:
        print("Le théorème est insuffisant")

    return retour

##
# Cette fonction est la fonction pour savoir dans un cas général
# si un graphe simple est Hamiltonien ou non (backtrack)
#
# Attention : cette fonction prend du temps.
#
# \param g le graphe simple dont on cherche à savoir s'il est hamiltonien.
# \return True si le graphe est Hamiltonien, False sinon.
def General(g, mode):

    if (mode == Verbose):
        print("Utilisation du backtrack \n")
    l = list()
    for i in range(0,g.num_verts()):
        l.append(-1)
    l[0]=0

    b = CycleHamiltonien(g,l,1)
    if(b == True and mode == Certificat):
        ca = list()
        for i in l:
            ca.append(g.vertices()[i])
        ca.append(g.vertices()[0])
        print(ca)
    return b    
    
        

def CycleHamiltonien(g, chemin, position):
    if (mode == Verbose):
        print("Test position " + str(position) +  "\n")

    if (position == g.num_verts()):
        if (g.vertices()[0] in g[g.vertices()[chemin[position-1]]]):
            if (mode == Verbose):
                print("Le graphe est hamiltonien")
            return True
        else :
            if (mode == Verbose):
                print("Pas de cycle")
            return False

    l = g.vertices()
    for i in range(0,g.num_verts()):
        if (g.vertices()[i] in g[g.vertices()[chemin[position-1]]] and i not in chemin) :
            chemin[position]=i
            
            if(CycleHamiltonien(g,chemin,position+1) == True):
                return True;

            chemin[position] = -1
            
    return False
    

    


##
# Cette fonction permet de savoir si un graphe est Hamiltonien ou non,
# l'ordre des fonctions de vérification s'effectue de la plus de la plus
# spécifique à la plus générale, en effet, chaque niveau de généralité
# implique des calculs plus longs.
#
# \param g le graphe simple dont on cherche a savoir s'il est Hamiltonien.
# \return True si le graphe est Hamiltonien, False sinon.
def solve(g, mode) :
    if (mode != Certificat):
        if simple(g, mode) == False :
            return False
        elif Dirac(g, mode) == True :
            return True
        elif Ore(g, mode) == True : 
            return True
        elif Posa(g, mode) == True :
            return True
        else :
            return General(g, mode)
    else :
        return General(g, mode)

def _decode_list(data):
    rv = []
    for item in data:
        if isinstance(item, unicode):
            item = item.encode('utf-8')
        elif isinstance(item, list):
            item = _decode_list(item)
        elif isinstance(item, dict):
            item = _decode_dict(item)
        rv.append(item)
    return rv

def _decode_dict(data):
    rv = {}
    for key, value in data.iteritems():
        if isinstance(key, unicode):
            key = key.encode('utf-8')
        if isinstance(value, unicode):
            value = value.encode('utf-8')
        elif isinstance(value, list):
            value = _decode_list(value)
        elif isinstance(value, dict):
            value = _decode_dict(value)
        rv[key] = value
    return rv
#
if __name__ == '__main__':
    cpt = 1
    mode = 0
    fin = False
    while cpt<len(argv) and not fin:
        if argv[cpt] == '-h' or argv[cpt] == '--help':
            print("Help for solver.py :")
            print("usage : sage solver.py [options] arg") 
            print("Options and arguments :")  
            print("-c --certificat : find a certificat")
            print("-v --verbose    : verbose (trace import statements)")
            print("-h --help       : print this help message and exit")
            print("arg             : a file which represent a simple graph")
            fin = True
        if argv[cpt] == '-v' or argv[cpt] == '--verbose':
            print("Mode verbose :")
            mode = Verbose
            cpt = cpt + 1
        elif argv[cpt] == '-c' or argv[cpt] == '--certificat':
            print("Certificat activé")
            mode = Certificat
            cpt = cpt + 1
        else:
            src = open(argv[cpt]).read()
            import json
            data = json.loads(src, object_hook=_decode_dict)
            g = Graph()
            g.add_vertices(data['vertices'])
            g.add_edges(data['edges'])
            l = solve(g, mode)
            if l == True :
                sys.exit("0")
            else :
                sys.exit("1")
            fin = True
            

   
