#!/bin/bash

#instance p1 valide
echo "Test P1 valide"
sage verificateur.sage graphe_hamiltonien_vrai ca_graphe_hamiltonien_vrai

#instance p1 invalide
echo "test P1 invalide"
sage verificateur.sage graphe_hamiltonien_faux ca_graphe_hamiltonien_vrai

#Reducteur p2 vrai + test
echo "reducteur P2 valide"
sage reducteur.py graphe_a_reduire_vrai
echo "Test P2 valide"
sage verificateur.sage essai ca_reduit

#Reducteur p2 faux + test
echo "reducteur P2 invalide"
sage reducteur.py graphe_a_reduire_faux
echo "Test P2 invalide"
sage verificateur.sage essai ca_reduit

#Solver p1 valide
echo "Solver P1 valide"
sage solver.py graphe_hamiltonien_vrai

#Solver p1 invalide
echo "Solver P1 invalide"
sage solver.py graphe_hamiltonien_faux
