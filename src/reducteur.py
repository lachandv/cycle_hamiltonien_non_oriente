#!/usr/bin/python
# -*- coding: utf-8-unix -*-

from sage.all import Graph
from sys import argv
import graphJson

##
# Début class Gadget

class Gadget:
	
	def __init__( self, vertexLeft, vertexRight ) :
		self.vertexLeft = vertexLeft
		self.vertexRight = vertexRight
		self.previousLeft = None
		self.nextLeft = None
		self.previousRight = None
		self.nextRight = None

# Fin class Gagdet
##

def createGadgets(g):
	
	listGadgets = list()
	for i in g.edges():
		listGadgets.append( Gadget( i[0], i[1] ) )
	return listGadgets


def reduct( listGadgets, k):

	GrapheOut = Graph()
	
	#Mise en place des antécédents et des suivants
	cptI = 0
	for i in listGadgets:
		cptJ = 0
		for j in listGadgets:
			if i != j and i > j :
				if i.vertexLeft == j.vertexLeft:
					i.nextLeft = (j.vertexLeft,'l1', cptJ)
					j.previousLeft = (i.vertexLeft,'l6', cptI)
				if i.vertexLeft == j.vertexRight:
					i.nextLeft = (j.vertexRight,'r1', cptJ)
					j.previousRight = (i.vertexLeft,'l6', cptI)
				if i.vertexRight == j.vertexLeft:
					i.nextRight = (j.vertexLeft,'l1', cptJ)
					j.previousLeft = (i.vertexRight,'r6', cptI)
				if i.vertexRight == j.vertexRight:
					i.nextRight = (j.vertexRight,'r1', cptJ)
					j.previousRight = (i.vertexRight,'r6', cptI)
			cptJ = cptJ + 1
		cptI = cptI + 1
	
	#Création du graphe de retour
	for i in range(0,k):
		GrapheOut.add_vertex(str(i))		
		
	cpt = 0
	for i in listGadgets:
		left = i.vertexLeft
		right = i.vertexRight
		GrapheOut.add_vertices( [ 'l'+str(cpt)+left+'1', 'l'+str(cpt)+left+'2', 'l'+str(cpt)+left+'3', 'l'+str(cpt)+left+'4', 'l'+str(cpt)+left+'5', 'l'+str(cpt)+left+'6' ] )
		GrapheOut.add_vertices( [ 'r'+str(cpt)+right+'1', 'r'+str(cpt)+right+'2', 'r'+str(cpt)+right+'3', 'r'+str(cpt)+right+'4', 'r'+str(cpt)+right+'5', 'r'+str(cpt)+right+'6' ] )
		cpt = cpt + 1
		
	cpt = 0
	for i in listGadgets:
		left = i.vertexLeft
		right = i.vertexRight
		GrapheOut.add_edge( [ 'l'+str(cpt)+left+'1', 'l'+str(cpt)+left+'2' ] )
		GrapheOut.add_edge( [ 'l'+str(cpt)+left+'1', 'r'+str(cpt)+right+'3' ] )
		GrapheOut.add_edge( [ 'l'+str(cpt)+left+'2', 'l'+str(cpt)+left+'3' ] )
		GrapheOut.add_edge( [ 'l'+str(cpt)+left+'3', 'l'+str(cpt)+left+'4' ] )
		GrapheOut.add_edge( [ 'l'+str(cpt)+left+'3', 'r'+str(cpt)+right+'1' ] )
		GrapheOut.add_edge( [ 'l'+str(cpt)+left+'4', 'l'+str(cpt)+left+'5' ] )
		GrapheOut.add_edge( [ 'l'+str(cpt)+left+'4', 'r'+str(cpt)+right+'6' ] )
		GrapheOut.add_edge( [ 'l'+str(cpt)+left+'5', 'l'+str(cpt)+left+'6' ] )
		GrapheOut.add_edge( [ 'l'+str(cpt)+left+'6', 'r'+str(cpt)+right+'4' ] )
		
		GrapheOut.add_edge( [ 'r'+str(cpt)+right+'1', 'r'+str(cpt)+right+'2' ] )
		GrapheOut.add_edge( [ 'r'+str(cpt)+right+'2', 'r'+str(cpt)+right+'3' ] )
		GrapheOut.add_edge( [ 'r'+str(cpt)+right+'3', 'r'+str(cpt)+right+'4' ] )
		GrapheOut.add_edge( [ 'r'+str(cpt)+right+'4', 'r'+str(cpt)+right+'5' ] )
		GrapheOut.add_edge( [ 'r'+str(cpt)+right+'5', 'r'+str(cpt)+right+'6' ] )
		cpt = cpt + 1

	cpt = 0
	for i in listGadgets:
		for j in range(0,k):
			if i.previousLeft == None:
				GrapheOut.add_edge( [ 'l'+str(cpt)+i.vertexLeft+'1',str(j) ] )
			if i.previousRight == None:
				GrapheOut.add_edge( [ 'r'+str(cpt)+i.vertexRight+'1',str(j) ] )
			if i.nextLeft == None:
				GrapheOut.add_edge( [ 'l'+str(cpt)+i.vertexLeft+'6',str(j) ] )
			if i.nextRight == None:
				GrapheOut.add_edge( [ 'r'+str(cpt)+i.vertexRight+'6',str(j) ] )
		
		if not(i.nextLeft == None):
			if i.nextLeft[1] == 'l1':
				GrapheOut.add_edge( [ 'l'+str(cpt)+i.vertexLeft+'6', 'l'+str(i.nextLeft[2])+i.nextLeft[0]+'1' ] )
			elif i.nextLeft[1] == 'r1':
				GrapheOut.add_edge( [ 'l'+str(cpt)+i.vertexLeft+'6', 'r'+str(i.nextLeft[2])+i.nextLeft[0]+'1' ] )

		if not(i.nextRight == None):
			if i.nextRight[1] == 'l1':
				GrapheOut.add_edge( [ 'r'+str(cpt)+i.vertexRight+'6', 'l'+str(i.nextRight[2])+i.nextRight[0]+'1' ] )
			elif i.nextLeft[1] == 'r1':
				GrapheOut.add_edge( [ 'r'+str(cpt)+i.vertexRight+'6', 'r'+str(i.nextRight[2])+i.nextRight[0]+'1' ] )
			
		cpt = cpt + 1
		
	cpt = cpt -1
	gadgetMax = listGadgets[len(listGadgets)-1]
	for j in range(0,k):
		if gadgetMax.nextLeft == None:
			GrapheOut.add_edge( [ 'l'+str(cpt)+gadgetMax.vertexLeft+'6',str(j) ] )
		if gadgetMax.nextRight == None:
			GrapheOut.add_edge( [ 'r'+str(cpt)+gadgetMax.vertexRight+'6',str(j) ] )
		
	return GrapheOut
	
	
def _decode_list(data):
    rv = []
    for item in data:
        if isinstance(item, unicode):
            item = item.encode('utf-8')
        elif isinstance(item, list):
            item = _decode_list(item)
        elif isinstance(item, dict):
            item = _decode_dict(item)
        rv.append(item)
    return rv

def _decode_dict(data):
    rv = {}
    for key, value in data.iteritems():
        if isinstance(key, unicode):
            key = key.encode('utf-8')
        if isinstance(value, unicode):
            value = value.encode('utf-8')
        elif isinstance(value, list):
            value = _decode_list(value)
        elif isinstance(value, dict):
            value = _decode_dict(value)
        rv[key] = value
    return rv

if __name__ == '__main__':
        cpt = 1
        mode = 0
        fin = False
        while cpt<len(argv) and not fin:
                if argv[cpt] == '-h' or argv[cpt] == '--help':
                        print("Help for solver.py :")
                        print("usage : sage solver.py [options] arg") 
                        print("Options and arguments :")  
                        print("-c --certificat : find a certificat")
                        print("-v --verbose    : verbose (trace import statements)")
                        print("-h --help       : print this help message and exit")
                        print("arg             : a file which represent a simple graph")
                        fin = True

                if argv[cpt] == '-v' or argv[cpt] == '--verbose':
                        print("Mode verbose :")
                        mode = Verbose
                        cpt = cpt + 1

                else:
                        src = open(argv[cpt]).read()
                        import json
                        data = json.loads(src, object_hook=_decode_dict)
                        g = Graph()
                        g.add_vertices(data['vertices'])
                        g.add_edges(data['edges'])
                        listGadgets = createGadgets(g)
                        graph = Graph()
                        graph = reduct(listGadgets, 2)
                        sortie=graphJson.graph_to_JSON(graph,"essai")
                        f = open('essai', 'w')
                        f.write(sortie)
                        f.close()
                        exit("essai")
		
